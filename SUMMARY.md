# Summary

## Kubernetes - LAB-Environment

[//]: #  ()

- [Installation](md/installation/README.md)
    * [OS Installation](md/installation/centos.md)
    * [Clone Worker Node](md/installation/clone.md)
    * [Kubernetes Setup](md/installation/clusterinit.md)
            
[//]: #  ()

- [Dashboard](md/dashboard/README.md)
    * [Zertifikate](md/dashboard/certificate.md)
    * [Dashboard](md/dashboard/dashboard.md)

    [//]: #  ()

- [Volumes](md/volumes/README.md)
    * [NFS Server](md/volumes/nfsserver.md)
    * [Persistent Volume](md/volumes/persistent.md)
    * [Persistent Volume Claim](md/volumes/claim.md)

[//]: #  ()

- [Deployment](md/deployment/README.md)
    * [Nginx](md/deployment/nginx.md)
    * [MySQL](md/deployment/mysql.md)
    * [Jenkins](md/deployment/jenkins.md)

[//]: #  ()

- [Secrets](md/secrets/README.md)
    * [Secrets](md/secrets/einleitung.md)

[//]: #  ()

- [k8s Administration](md/administration/README.md)
    * [kubeadm](md/administration/kubeadm.md)
    * [kubectl](md/administration/kubectl.md)    


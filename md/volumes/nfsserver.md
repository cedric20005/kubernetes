[1]: https://www.tecmint.com/install-a-kubernetes-cluster-on-centos-8/
[2]: https://www.howtoforge.com/tutorial/centos-kubernetes-docker-cluster/
[3]: https://download.docker.com/linux/centos/8/x86_64/stable/Packages/
[4]: https://github.com/flannel-io/flannel
[5]: https://www.weave.works/docs/net/latest/overview/
[6]: https://stackoverflow.com/questions/47845739/configuring-flannel-to-use-a-non-default-interface-in-kubernetes
[7]: http://linuxsoft.cern.ch/centos/8-stream/isos/x86_64/
[8]: https://www.centos.org/
[9]: https://github.com/kubernetes/dashboard/blob/master/docs/user/installation.md "Official Kubernets Dashboard"
[10]: https://www.rosehosting.com/blog/how-to-generate-a-self-signed-ssl-certificate-on-linux/ "Self-Signed Certificate"
[11]: https://www.tecmint.com/install-nfs-server-on-centos-8
[15]: https://www.jenkins.io/doc/book/installing/kubernetes/


[51]: https://kubernetes.io/docs/concepts/storage/volumes/
[52]: https://kubernetes.io/docs/concepts/storage/persistent-volumes/
[70]: https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/
[90]: https://kubernetes.io/docs/tasks/run-application/run-single-instance-stateful-application/

# NFS Server 

Für die persistente Datenspeicherung in unserem Kubernetes Cluster verwenden wir **NFS Shares**, welche für *PersistentVolume* verwendet werden.


## NFS Server und Client Setup
Das NFS-Setup wird anhand dieser [Anleitung][11] erstellt.

### Environment

* Centos8 VM
* Host-Only Network 192.168.11.100
* NFS installed

## NFS Server

### Installation

```
# dnf install -y nfs-utils
# systemctl start nfs-server.service
# systemctl enable nfs-server.service
```

### Firewall

NFS Firewall Rules zufügen

```
# firewall-cmd --permanent --add-service=nfs
# firewall-cmd --permanent --add-service=rpc-bind
# firewall-cmd --permanent --add-service=mountd
# firewall-cmd --reload
```

oder alternativ Firewall stoppen und deaktivieren wenn keine Firewall verwendet werden soll.

```
# systemctl stop firewalld
# systemctl disable firewalld
```

### NFS Konfigurieren

Konfigurationsfile

* /etc/nfs.conf – main configuration file for the NFS daemons and tools.
* /etc/nfsmount.conf – an NFS mount configuration file.
* /etc/exports


### NFS Exports

Für die LAB-Umgebung werden folgende Exports unter `/mnt/nfs_shares` erstellt. Diese entsprechen den Kubernetes Storage-Klassen

* platin
* gold
* silver
* mysql

```
# mkdir -p  /mnt/nfs_shares/platin
# mkdir -p  /mnt/nfs_shares/gold
# mkdir -p  /mnt/nfs_shares/silver
```

Die Filesysteme in `/etc/exports` für für die NFS-Freigabe definieren

> `no_root_squash` erlaubt es `root` auf die Shares zu schreiben

```
# cat <<EOF>> /etc/exports
/mnt/nfs_shares/platin     192.168.11.0/24(rw,sync,no_root_squash)
/mnt/nfs_shares/gold       192.168.11.0/24(rw,sync,no_root_squash)
/mnt/nfs_shares/silver     192.168.11.0/24(rw,sync,no_root_squash)
EOF
```

Mit `exportfs` exportieren

```
# exportfs -arv
exporting 192.168.11.0/24:/mnt/nfs_shares/silver
exporting 192.168.11.0/24:/mnt/nfs_shares/gold
exporting 192.168.11.0/24:/mnt/nfs_shares/platin
```

Aktuelle Freigabe anzeigen

```
# exportfs -s
/mnt/nfs_shares/platin  192.168.11.0/24(sync,wdelay,hide,no_subtree_check,sec=sys,rw,secure,root_squash,no_all_squash)
/mnt/nfs_shares/gold    192.168.11.0/24(sync,wdelay,hide,no_subtree_check,sec=sys,rw,secure,root_squash,no_all_squash)
/mnt/nfs_shares/silver  192.168.11.0/24(sync,wdelay,hide,no_subtree_check,sec=sys,rw,secure,root_squash,no_all_squash)
```


## NFS Client

Der *Master-Node* sowie die beiden *Worker-Nodes* werden als *NFS-Client* konfiguriert.

### Installation

NFS Utils installieren

```
# dnf install -y nfs-utils nfs4-acl-tools
```

Mit `showmount -e` verfügbare NFS Shares auf `192.168.11.100` anzeigen.

### NFS Server in Hosts eintragen

```
# cat <<EOF>> /etc/hosts
192.168.11.100 nfsserver
EOF
```

### NFS testen
```
# showmount -e nfsserver
Export list for nfsserver:
/mnt/nfs_shares/silver 192.168.11.0/24
/mnt/nfs_shares/gold   192.168.11.0/24
/mnt/nfs_shares/platin 192.168.11.0/24
```

[zurück](README.md) | [weiter](persistent.md) | [Home](../../README.md)
[1]: https://www.tecmint.com/install-a-kubernetes-cluster-on-centos-8/
[2]: https://www.howtoforge.com/tutorial/centos-kubernetes-docker-cluster/
[3]: https://download.docker.com/linux/centos/8/x86_64/stable/Packages/
[4]: https://github.com/flannel-io/flannel
[5]: https://www.weave.works/docs/net/latest/overview/
[6]: https://stackoverflow.com/questions/47845739/configuring-flannel-to-use-a-non-default-interface-in-kubernetes
[7]: http://linuxsoft.cern.ch/centos/8-stream/isos/x86_64/
[8]: https://www.centos.org/
[9]: https://github.com/kubernetes/dashboard/blob/master/docs/user/installation.md "Official Kubernets Dashboard"
[10]: https://www.rosehosting.com/blog/how-to-generate-a-self-signed-ssl-certificate-on-linux/ "Self-Signed Certificate"
[11]: https://www.tecmint.com/install-nfs-server-on-centos-8
[15]: https://www.jenkins.io/doc/book/installing/kubernetes/


[51]: https://kubernetes.io/docs/concepts/storage/volumes/
[52]: https://kubernetes.io/docs/concepts/storage/persistent-volumes/
[70]: https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/
[90]: https://kubernetes.io/docs/tasks/run-application/run-single-instance-stateful-application/

# Persistent Volume Claims

> Nachfolgende Schritte erfolgen auf dem *Master-Node*

## PVC Definition

Die *PersistentVolumeClaim* Definitionen unter `$HOME/volumes` speichern

```
# cd $HOME/volumes
```

*Persistent Volume Claim* Definition `pvc-nfs.yaml` erstellen

```
# cat <<EOF>> /$HOME/volumes/pvc-nfs.yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-nfs-silver
spec:
  storageClassName: silver
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 2Gi

---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-nfs-gold
spec:
  storageClassName: gold
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Gi

---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-nfs-platin
spec:
  storageClassName: platin
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Gi
EOF
```

### PVC - applizieren

*Persistent Volume Claim* von `pvc-nfs.yaml` applizieren

```
# kubectl apply -f pvc-nfs.yaml
persistentvolumeclaim/pvc-nfs-silver created
persistentvolumeclaim/pvc-nfs-gold created
persistentvolumeclaim/pvc-nfs-platin created
```

Analog können die *Persistent Volume Claim* mit `pvc-nfs.yaml` wieder gelöscht werden

```
# kubectl delete -f pvc-nfs.yaml
persistentvolumeclaim "pvc-nfs-silver" deleted
persistentvolumeclaim "pvc-nfs-gold" deleted
persistentvolumeclaim "pvc-nfs-platin" deleted
```

[zurück](persistent.md) | [Home](../../README.md)


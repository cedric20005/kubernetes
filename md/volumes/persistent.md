[1]: https://www.tecmint.com/install-a-kubernetes-cluster-on-centos-8/
[2]: https://www.howtoforge.com/tutorial/centos-kubernetes-docker-cluster/
[3]: https://download.docker.com/linux/centos/8/x86_64/stable/Packages/
[4]: https://github.com/flannel-io/flannel
[5]: https://www.weave.works/docs/net/latest/overview/
[6]: https://stackoverflow.com/questions/47845739/configuring-flannel-to-use-a-non-default-interface-in-kubernetes
[7]: http://linuxsoft.cern.ch/centos/8-stream/isos/x86_64/
[8]: https://www.centos.org/
[9]: https://github.com/kubernetes/dashboard/blob/master/docs/user/installation.md "Official Kubernets Dashboard"
[10]: https://www.rosehosting.com/blog/how-to-generate-a-self-signed-ssl-certificate-on-linux/ "Self-Signed Certificate"
[11]: https://www.tecmint.com/install-nfs-server-on-centos-8
[15]: https://www.jenkins.io/doc/book/installing/kubernetes/


[51]: https://kubernetes.io/docs/concepts/storage/volumes/
[52]: https://kubernetes.io/docs/concepts/storage/persistent-volumes/
[70]: https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/
[90]: https://kubernetes.io/docs/tasks/run-application/run-single-instance-stateful-application/


# NFS basierende PersitentVolumes

Nachfolgende Schritte erfolgen auf dem *Master-Node*

## PersistentVolume Definition

Die *PersistentVolumes* Definitionen unter `$HOME/volumes` speichern

```
# mkdir $HOME/volumes
# cd $HOME/volumes
```

*Persistent Volume* Definition `pv-nfs.yaml` erstellen

```
# cat <<EOF>> /$HOME/volumes/pv-nfs.yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-nfs-platin
spec:
  capacity:
    storage: 1Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteMany
  persistentVolumeReclaimPolicy: Recycle
  storageClassName: platin
  nfs:
    server: nfsserver
    path: "/mnt/nfs_shares/platin"
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-nfs-gold
spec:
  capacity:
    storage: 5Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteMany
  persistentVolumeReclaimPolicy: Recycle
  storageClassName: gold
  nfs:
    server: nfsserver
    path: "/mnt/nfs_shares/gold"
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-nfs-silver
spec:
  capacity:
    storage: 5Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteMany
  persistentVolumeReclaimPolicy: Recycle
  storageClassName: silver
  nfs:
    server: nfsserver
    path: "/mnt/nfs_shares/silver"
EOF
```

## PersistentVolume applizieren

Die in `pv-nfs.yaml` definierten *PersistentVolume* applizieren

```
# kubectl apply -f pv-nfs.yaml
persistentvolume/pv-nfs-platin created
persistentvolume/pv-nfs-gold created
persistentvolume/pv-nfs-silver created
```

Analog können die *PersistentVolume* mit `kubectl delete` wieder gelöscht werden

```
# kubectl delete -f pv-nfs.yaml
persistentvolume "pv-nfs-platin" deleted
persistentvolume "pv-nfs-gold" deleted
persistentvolume "pv-nfs-silver" deleted
```

> alle *PersistentVolume* sind hier in einem File zusammengefasst. In der Praxis ist das nicht empfohlen.

[zurück](nfsserver.md) | [weiter](claim.md) | [Home](../../README.md)
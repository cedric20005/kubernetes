#  Kubernetes Installation (k8s) auf CentOS 8 Server

1. [OS & Software Installation](centos.md)
2. [Clone Worker Node](clone.md)
3. [Kubernetes Setup](md/installation/clusterinit.md)

[zurück](../../README.md)
# LB3 - Kubernetes 

Für eine speditive Bewertung führen sie bitte alle nachfolgenden Befehle auf ihrer lokalen Installation aus und halte die Resultate in einer Text-Datei fest. 

Falls sie über den Umfang der Anleitung hinaus weitere Konfigurationen (etwa weitere Deployment) vorgenommen haben, halten sie diese ebenfalls in dieser Textdate fest. 

Benennen sie die Textdatei wie folgt: `lb2_Vorname_Nachname.txt`

> Senden sie die Datei per Mail an **marco.berger@tbz.ch**


## Befehle

**UUID**

`kubectl describe nodes |grep ID:`

**Cluster Node**

`kubectl get node -o wide`

**Pods**

`kubectl get pod --all-namespaces`

**Service**

`kubectl get service --all-namespaces`

**Deployment**

`kubectl get deployments.apps`

**Persistent Volume**

`kubectl get persistentvolume`

## Weitere Konfigurationen

```
möglichst genaue Beschreibung welche weiteren Konfigurationen sie vorgenommen haben
```



